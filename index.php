<?php

// подключаем классы
require __DIR__ . '/lib/models/TextFile.php';
require __DIR__ . '/lib/models/GuestBook.php';
require __DIR__ . '/lib/classes/View.php';

$template = __DIR__ . '/lib/templates/guestBookMain.php';
$gbPath = __DIR__ . '/lib/data/gbData.txt';

// создаем объекты
$guestBook = new GuestBook($gbPath);
$view = new View;

// заполняем данные и выводим шаблон
$view
    ->assign('records', $guestBook->getAllRecords())
    ->assign('title', 'Гостевая книга')
    ->assign('h1', 'GuestBook')
    ->display($template);
