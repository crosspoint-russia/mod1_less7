<?php

// подключаем классы
require __DIR__ . '/../lib/models/TextFile.php';
require __DIR__ . '/../lib/models/News.php';
require __DIR__ . '/../lib/classes/View.php';

$template = __DIR__ . '/../lib/templates/newsArticle.php';
$newsPath = __DIR__ . '/../lib/data/newsData.php';

// создаем объекты
$news = new News($newsPath);
$view = new View;

// заполняем данные и выводим шаблон
if (isset($_GET['id'])) {

    $article = $news->getRecordById($_GET['id']);

    // проверяем наличие объекта с новостью
    if ($article instanceof Article) {
        $view
            ->assign('article', $article)
            ->display($template);
    } else {
        header('HTTP/1.0 404 Not Found');
        die();
    }

} else {
    header('Location: /news.php');
    die();
}
