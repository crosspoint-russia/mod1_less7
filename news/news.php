<?php

// подключаем классы
require __DIR__ . '/../lib/models/TextFile.php';
require __DIR__ . '/../lib/models/News.php';
require __DIR__ . '/../lib/classes/View.php';

$template = __DIR__ . '/../lib/templates/newsMain.php';
$newsPath = __DIR__ . '/../lib/data/newsData.php';

// создаем объекты
$news = new News($newsPath);
$view = new View;

// заполняем данные и выводим шаблон
$view
    ->assign('records', $news->getAllRecords())
    ->assign('title', 'Новости')
    ->assign('h1', 'Список новостей:')
    ->display($template);
