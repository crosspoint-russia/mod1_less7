<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?php echo $this->data['title']; ?></title>
</head>
<body>

<h1><?php echo $this->data['h1']; ?></h1>
<ul>
    
<?php foreach ($this->data['records'] as $record) : ?>
    <li><?php echo $record->getText(); ?></li>
<?php endforeach; ?>
    
</ul>
<form action="/saveGbRecord.php" method="post">
    <input type="text" name="message">
    <button submit="submit">Send</button>
</form>
<ul>
    <li><a href="/fileUploadDemo/fileUploadDemo.php">File upload demo</a></li>
    <li><a href="/news/news.php">News class demo</a></li>
</ul>
</body>
</html>