<?php
$article = $this->data['article'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $article->getTitle(); ?></title>
</head>
<body>

<h1><?php echo $article->getTitle(); ?></h1>

<p>
    <?php echo $article->getFullStory(); ?>
</p>

<a href="/news/news.php">Return to all news</a>

</body>
</html>