<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $this->data['title']; ?></title>
</head>
<body>

<h1><?php echo $this->data['h1']; ?></h1>

<?php if (!is_null($this->data['uploadedFile'])): ?>
    <p>
        <?php echo $this->data['uploadedFile']; ?>
    </p>
<? endif; ?>

<p>
    <a href="/">Return to main page</a>
</p>

</body>
</html>