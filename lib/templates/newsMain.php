<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $this->data['title']; ?></title>
</head>
<body>

<h1><?php echo $this->data['h1']; ?></h1>

<ul>
    <?php foreach ($this->data['records'] as $key => $article) : ?>
    <li>
        <a href="/news/article.php?id=<?php echo ++$key; ?>"><?php echo $article->getTitle(); ?></a>
        <p>
            <?php echo $article->getShortStory(); ?>
        </p>
    </li>
    <?php endforeach; ?>
</ul>

<p>
    <a href="/">Return to main page</a>
</p>

</body>
</html>