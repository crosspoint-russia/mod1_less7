<?php

require_once __DIR__ . '/GuestBookRecord.php';

/**
 * Создайте класс GuestBook, который будет удовлетворять следующим требованиям:
 * В конструктор передается путь до файла с данными гостевой книги, в нём же происходит чтение данных из ней (используйте защищенное свойство объекта для хранения данных)
 * Метод getData() возвращает массив записей гостевой книги
 * Метод append($text) добавляет новую запись к массиву записей
 * Метод save() сохраняет массив в файл
 */
class GuestBook extends TextFile
{
    public function __construct($path)
    {
        parent::__construct($path);

        $records = file($this->path, FILE_IGNORE_NEW_LINES);
        foreach ($records as $record) {
            $this->records[] = new GuestBookRecord($record);
        }
    }

    public function append(GuestBookRecord $record)
    {
        $this->records[] = $record;
        return $this;
    }

    public function save()
    {
        // получаем массив строк
        $tempRecordsArray = [];

        foreach ($this->records as $record) {
            // проверка на принадлежность к классу
            if ($record instanceof GuestBookRecord) {
                $tempRecordsArray[] = $record->getText();
            }
        }

        // клеим в строку и сохраняем в файл
        $dataString = implode("\n", $tempRecordsArray);
        file_put_contents($this->path, $dataString);
    }

}