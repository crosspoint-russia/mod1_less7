<?php

class Article
{
    protected $title;
    protected $shortStory;
    protected $fullStory;

    public function __construct($title, $shortStory, $fullStory)
    {
        $this->title = $title;
        $this->shortStory = $shortStory;
        $this->fullStory = $fullStory;
    }

    // Серия геттеров

    public function getTitle()
    {
        return $this->title;
    }

    public function getShortStory()
    {
        return $this->shortStory;
    }

    public function getFullStory()
    {
        return $this->fullStory;
    }
    
}