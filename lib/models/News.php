<?php

require_once __DIR__ . '/Article.php';

class News extends TextFile
{
    protected $path;
    protected $records = [];

    public function __construct($path)
    {
        parent::__construct($path);

        $news = include($this->path);

        foreach ($news as $record) {
            $this->records[] = new Article($record['title'], $record['shortStory'], $record['fullStory']);
        }
    }

    // метод для получения записи по id
    public function getRecordById($id)
    {
        // проверяем наличие записи по ключу
        if (isset($this->records[$id-1])) {
            return $this->records[$id-1];
        } else {
            return null; // если записи нет возвращаем null
        }

    }

}