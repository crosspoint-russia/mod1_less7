<?php

// подключаем классы
require __DIR__ . '/lib/models/TextFile.php';
require __DIR__ . '/lib/models/GuestBook.php';

// создаем объект
$gbPath = __DIR__ . '/lib/data/gbData.txt';
$guestBook = new GuestBook($gbPath);

if (isset($_POST['message']) && '' != $_POST['message']) {

    $gbRecord = new GuestBookRecord($_POST['message']);
    $guestBook
        ->append($gbRecord)
        ->save();

}
header('Location: /');
