<?php

// подключаем классы
require __DIR__ . '/../lib/classes/View.php';

$template = __DIR__ . '/../lib/templates/fileUploadDemo.php';

// создаем объекты
$view = new View;

// заполняем данные и выводим шаблон
$view
    ->assign('title', 'Загрузка файлов')
    ->assign('h1', 'File upload')
    ->display($template);

