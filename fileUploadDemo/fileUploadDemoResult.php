<?php

// подключаем классы
require __DIR__ . '/../lib/classes/Uploader.php';
require __DIR__ . '/../lib/classes/View.php';

$template = __DIR__ . '/../lib/templates/fileUploadDemoResult.php';
$savePath = __DIR__ . '/../lib/uploads';

// создаем объекты
$userUpload = new Uploader('userFile');
$view = new View;

// загрузка файла
$result = $userUpload->setSavePath($savePath)->upload();

// заполняем данные и выводим шаблон
$view
    ->assign('uploadedFile', $result)
    ->assign('title', 'Результат загрузки файла')
    ->assign('h1', 'File upload result:')
    ->display($template);